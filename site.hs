--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications  #-}
import           Control.Arrow
import           Control.Monad
import           Data.Char
import           Data.Function
import           Data.Functor ((<&>))
import           Data.Maybe
import           Data.Monoid          (mappend)
import           Data.String
import           Hakyll
import           Hakyll.Core.Provider
import qualified Hakyll.Core.Store    as Store
import           Safe
import           System.FilePath
import qualified GHC.IO.Encoding as E


--------------------------------------------------------------------------
config :: Configuration
config = defaultConfiguration {
    destinationDirectory = "public"
}

main :: IO ()
main = do
  E.setLocaleEncoding E.utf8 -- Was getting `commitBuffer: invalid argument (invalid character)` error https://github.com/jaspervdj/hakyll/issues/109
  hakyllWith config $ do
    match "css/default.css" $ do
      route   idRoute
      compile compressCssCompiler

    match "content/*/*" $ do
      route   idRoute
      compile copyFileCompiler

    match "templates/*" $ compile templateBodyCompiler

    match "worksheets/*" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/worksheet.html" worksheetCtx
            >>= loadAndApplyTemplate "templates/default.html" worksheetCtx
            >>= relativizeUrls

    create ["index.html"] $ do
      route idRoute
      compile $ do
        worksheetsCtx <- mkWorksheetsContext

        makeItem ""
          >>= loadAndApplyTemplate "templates/worksheet-list.html" worksheetsCtx
          >>= loadAndApplyTemplate "templates/default.html" worksheetsCtx
          >>= relativizeUrls

utilityFunctionsCtx :: Context a
utilityFunctionsCtx
  = functionField "takeFileName" 
      (\xs _ -> pure (concatMap takeFileName xs))

mkWorksheetsContext :: Compiler (Context String)
mkWorksheetsContext = do
  worksheets <- loadAll "worksheets/*"
  pure $ mconcat
    [ listField "worksheets" worksheetCtx (return worksheets)
    , constField "title" "Worksheets"
    , defaultContext
    ]

worksheetCtx :: Context String
worksheetCtx = mconcat
  [ listFieldWith "resources" (utilityFunctionsCtx <> defaultContext) loadAllFromWeekDirectory
  , backAndNextFields
  , defaultContext
  ]

weeksList :: [String]
weeksList = ["03","04","05","06a","06b","07","08","09"
            ,"10","11","13","14","15","16","18","19","20","21","22","23"]

backAndNextFields :: Context a
backAndNextFields = mconcat
  [ field "backUrl" (getUrlUsing findBefore)
  , field "nextUrl" (getUrlUsing findAfter)
  ]
  where
    getUrlUsing findFrom item 
        = maybe "" (\week -> concat ["/worksheets/week", week, ".html"])
        . findFrom weeksList
      <$> getWeekField item

findBefore :: Eq a => [a] -> a -> Maybe a
findBefore xs x
  = if takeWhile (/= x) xs == xs
      then Nothing
      else takeWhile (/= x) xs & lastMay

findAfter :: Eq a => [a] -> a -> Maybe a
findAfter xs x = dropWhile (/= x) xs
               & drop 1
               & headMay

getWeekField :: Item a -> Compiler String
getWeekField = itemIdentifier >>> flip getMetadataField' "week"

loadAllFromWeekDirectory :: Item b -> Compiler [Item String]
loadAllFromWeekDirectory item = do
  filePath <- getResourceIdentifier item

  let pat = fromString 
          $ "content/" ++ takeBaseName filePath ++ "/*"
      
      ignoreItemContents = fmap (const "") -- Only interested in identifiers

  rs <- loadAll @CopyFile pat
  return $ map ignoreItemContents rs

  -- loadAll @CopyFile pat
  --   <&> map ignoreItemContents


getResourceIdentifier :: Item a -> Compiler String
getResourceIdentifier = itemIdentifier >>> toFilePath >>> pure

--------------------------------------------------------------------------------
