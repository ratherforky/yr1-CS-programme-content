---
week: "11"
title: Command line todo app in Haskell
worksheet: 9
---

This sheet is a response to the common complaint that students don't feel like they can write 'real' programs with Haskell after finishing the unit. It involves some file handling, dealing with user input, and running the correct commands in response. The hope is that it shows that students can take everything they know about pure functions and add a little bit of IO to make them into 'real' programs. Unfortunately it was right at the end of the term and on the same day as the C coursework deadline, so many will have missed it.