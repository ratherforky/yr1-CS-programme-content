---
week: "23"
title: Radix sort in Java
worksheet: 19
---

This sheet covers radix sort, a non-comparison based sorting algorithm that runs in O(n) time, with the limitation being it can only sort arrays of integers. The solutions attempt to make the conceptual steps in the algorthim more clear than is often found in example implementations online.
