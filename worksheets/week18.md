---
week: "18"
title: Heap sort in Java
worksheet: 14
---

This sheet covers heap sort as an example of an O(nlog n), unstable, in-place sorting algorithm.