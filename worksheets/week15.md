---
week: "15"
title: Merge sort in Java
worksheet: 12
---

This is the main true divide-and-conquer algorithm covered in the sheets. The solutions contain benchmarking annotations so that the difference between different implementations can be evaluated.