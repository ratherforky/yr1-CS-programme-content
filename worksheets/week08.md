---
week: "08"
title: Problem solving with Advent of Code and Haskell
worksheet: 6
---

This sheet involved solving two problems from scratch. In previous sheets, most of the 'big picture' problem solving was done for students by specifying what functions were needed to complete the task, while they were left to implement the individual functions. Instead for this week I wanted students to do all of the problem solving, with the help of their TA and each other. It also served to advertise the upcoming Advent of Code, which many students participated in, competing on our private leaderboard.

I included two different solutions to the tasks: one that I wrote in 2018 when the challenge was originally released, and one that I wrote in preparation for the seminars. I hoped this would demonstrate that there are many different ways to approach problems, and that your style will develop over time.