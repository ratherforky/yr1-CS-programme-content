---
week: "22"
title: Alpha-beta pruning
worksheet: 18
---

Building on the previous sheet, this introduces alpha-beta pruning to minimax, and this time focuses entirely on whiteboard exercises. A Haskell example of alpha-beta pruning being added to the previous noughts and crosses code is given without any associated tasks, because there wouldn't have been time to complete them but I'd already written the code for fun. The pruned version is noticeably faster at the start of the game when the game tree is largest.