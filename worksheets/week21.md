---
week: "21"
title: Minimax, with a side of Haskell
worksheet: 17
---

Minimax is not covered in the unit content, but it is encouraged in the coursework, so this sheet is all about getting a conceptual understanding of the algorithm. It is the first sheet to be more focused on whiteboard workings than on code. There is also an implementation task in Haskell (so there's no chance of students copying and pasting the code into their Java coursework), but there wasn't enough time to complete the whiteboard exercises and the coding exercises.

The Haskell code itself, when completed, lets you play a game of noughts and crosses in the terminal against your AI. You'll know your AI is correct if it's impossible to beat, because if your opponent plays optimally in noughts and crosses the best you can hope for is a draw.