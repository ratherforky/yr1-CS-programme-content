---
week: "09"
title: Type classes in Haskell
worksheet: 7
---

Controversially, many functions in Haskell's Prelude were rewritten to be more generalised. This was done using type classes, aka. Haskell's function overloading/ad-hoc polymorphism. For example, `sum :: [Int] -> Int` was changed to `sum :: (Foldable t, Num a) => t a -> a`. This made functions more flexible, but it also made them harder to understand for people just starting to learn the language. This sheet attempts to demystify some of the common type classes and show that they're not as scary as they might first appear.