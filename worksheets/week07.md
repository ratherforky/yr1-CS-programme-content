---
week: "07"
title: Deriving boolean expressions from truth tables in Haskell
worksheet: 5
---

This task involves translating a Computer Architecture algorithm to Haskell. In the process, it should improve Haskell skills and make students think more deeply about the algorithm itself, since they cannot directly translate pseudo-code. At this point, they'd encountered more higher-order functions and foldr, making the code much cleaner and (I think) clearer about what it's doing.