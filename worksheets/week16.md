---
week: "16"
title: Using the visitor pattern to fold trees in Java
worksheet: 13
---

This sheet covers the visitor pattern and links it to previous understandings of folds from Haskell. It starts with some concrete visitors, then asks students to incrementally abstract parts of the visitors using higher order functions and generics, to make their code more DRY. The final extension question asks students to make a fold method on the tree itself to drive home the connection.