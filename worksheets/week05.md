---
week: "05"
title: Writing recursive list functions in Haskell
worksheet: 3
---

This sheet was made in response to an overwhelming demand for help with Haskell. It deals primarily with lists and recursion, but also provides motivation for higher order functions towards the end of the sheet.