---
week: "14"
title: Binary search and making a Set class in Java
worksheet: 11
---

This sheet uses the motivation of creating a Set class to demonstrate the use of binary search. Unfortunately I made some pretty big mistakes in the solutions, particularly in the complexity analysis.Linked lists actually make everything slower and insertions cannot be made O(log n) using binary search, because even though we only need O(log n) comparisons, we still need O(n) swaps for actual insertion.
