---
week: "06b"
title: List zippers in Haskell
worksheet: 4b
---

This was a more straightforward programming task in Haskell and it was much more well recieved than the FSMs sheet. It introduces a data type similar to doubly-linked lists and asks students to implement the same functions they had to make for the C coursework happening at the time.