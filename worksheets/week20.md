---
week: "20"
title: Streams in Java
worksheet: 16
---

This worksheet is on Java's Streams. They're nice because they let you write Java in a more functional style and they make parallelism almost trivial (provided you obey all the requirements, which is easy to do if you're used to a pure FP language like Haskell). The task itself provides an in-memory database of films and asks you to make some queries from it using Streams. There is also an appendix to the sheet which lists useful Stream methods and translates the Java types into simplified Haskell-style types to remove some of the noise and make them more understandable.
