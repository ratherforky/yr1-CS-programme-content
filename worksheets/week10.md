---
week: "10"
title: Bitwise operations and function pointers in C
worksheet: 8
---

This sheet focused on bit operations in C (which I noticed many students were struggling with in their C coursework) and an example of higher order functions in C using `qsort()` (even though they're not as ergonomic or expressive as they are in Haskell, they are possible using function pointers and are touched on in the course content).