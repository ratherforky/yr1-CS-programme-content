---
week: "13"
title: Peak finding in Java
worksheet: 10
---

This sheet is about implementing the peak finding algorithm in Java. Like in many other subsequent sheets, it's an attempt to bridge the gap between the algorithms and OOP parts of the unit, so that students get a better understanding of both. 