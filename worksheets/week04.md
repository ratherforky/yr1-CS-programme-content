---
week: "04"
title: String manipulation in Haskell and C
worksheet: 2
---

This sheet is about writing the same functions in both C and Haskell to highlight the similarities and difference between the languages. Equivalent main functions are provided for both languages.