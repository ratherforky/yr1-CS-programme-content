---
week: "03"
title: Logic puzzles, function composition in Haskell, and binary addition in C
worksheet: 1
---

This sheet attempted to include introductory content from all the different core CS units. This proved intractable in later weeks.