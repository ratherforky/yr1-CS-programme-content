---
week: "06a"
title: Finite state machines in Haskell
worksheet: 4a
---

This was an attempt to have crossover with computer architecture content, but unfortunately this proved to be too complicated at this point of the course. The hope was to provide a practical example of FSMs from Computer Architecture and give some motivation for folds later on in the Haskell course, but the difficulty of both concepts at once made both harder to understand. This content might have been better received later in the course, after dealing with folds. A second worksheet was hastily created for the seminars in the afternoon. 