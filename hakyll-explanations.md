## Configuration

- Just a record
- `defaultConfiguration :: Configuration` can be updated with record syntax
- `destinationDirectory` field selects the folder where all the static files for the site will go

## Rules a

- Monad (newtype around RWST/Reader-writer-state monad, not too important)
- For composing rules
- `match :: Pattern -> Rules () -> Rules ()`
  - 'For identifiers/files matching the given pattern, run these rules'
- `route :: Routes -> Rules ()`
  - Make a rule which says where files should be routed to in the static site
- `create :: [Identifier] -> Rules () -> Rules ()`
  - Make new files

### Identifier

- More or less the file path to a resource
- In general, it doesn't change
- Different to a Route, ie. the place where a resource is saved in the static site

### Pattern

- Simple pattern for matching against Identifiers
- Not regex by default, but regex possible
- OverloadedStrings needed to use them ergonomically
- Simplest example for matching on one file: "css/default.css"
- Limited wildcard support
  - `"*"`: matches at most one element of an identifier;
    - eg. `"templates/*"` matches all files in templates directory
  - `"**"`: matches one or more elements of an identifier.
    - eg. `"content/**"` matches everything in the content directory and recursively all its subdirectories
    - eg. `"content/**/*.hs"` matches only the .hs files in content and its subdirectories

## Routes

- `idRoute :: Routes`
  - Simplest route, just uses the identifier as the filepath (relative to the site directory)
- `setExtension :: String -> Routes`
  - Set (or replace) the extension of a route
  - Useful for when you use a file of one kind and compile it to be another (eg. turning a markdown file into an html file)


## Compiler

- Monad that compiles items and takes care of dependency tracking
- `compile :: (Binary a, Typeable a, Writable a) => Compiler (Item a) -> Rules ()`
  - Add a compilation rule to the rules.
  - This instructs all resources to be compiled using the given compiler.
- `copyFileCompiler :: Compiler (Item CopyFile)`
  - Simplest, just copies a file to the correct route
  - `CopyFile` newtype for `FilePath`
- `compressCssCompiler`
  - Take CSS and make it smaller, eg. by removing unnecessary whitespace
- `pandocCompiler`
  - Use pandoc to convert a file into an html page
  - Other functions let you specify your own pandoc options
 

## Item

- Just a record with the data and an associated `Identifier`

## Context

- Contexts can be given to templates
- They contain fields, which can then be used as variables in the templating language
- Functions can also be given as fields
  - `functionField :: String	-> ([String] -> Item a -> Compiler String) -> Context a`
    - variadic function field, `[String]` is the list of arguments
    - `Item a` is the page currently being rendered
- List fields
  - Can be looped over in template
  - Each item of the list gets its own context

## Templates

- Field variables can be used inside `$ $`s 
