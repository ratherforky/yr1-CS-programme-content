# Year 1 Computer Science Programme Content

## Viewing the website

https://ratherforky.gitlab.io/yr1-CS-programme-content/

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Clone the repo using 'git clone --recurse-submodules https://gitlab.com/ratherforky/yr1-CS-programme-content.git` (most of the content is in another git repo)
2. Build Hakyll: `stack build`
3. Generate the website: `stack exec site build`
4. Preview your project: `stack exec site watch`
5. Add content

More useful commands:

- `stack exec site clean`, clears the cache and deletes the static site. Often useful since Hakyll isn't the best at detecting when dependencies are stale

Read more at Hakyll's [documentation][hakyll].

---

Forked from https://gitlab.com/jtojnar/hakyll

[ci]: https://about.gitlab.com/gitlab-ci/
[hakyll]: https://jaspervdj.be/hakyll/
[install]: https://jaspervdj.be/hakyll/tutorials/01-installation.html
[documentation]: http://link-to-main-documentation-page
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
